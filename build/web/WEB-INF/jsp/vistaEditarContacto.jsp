<%-- 
    Document   : vistaEditarContacto
    Created on : 24/05/2021, 07:53:19 PM
    Author     : kokob
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Contacto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="../css/style.css"/>
    </head>
    <body class="form-v10">
        <div class="page-content">
            <div class="form-v10-content">
                <form class="form-detail" action="#" method="post" id="myform">
                    <div class="form-left">
                        <h2>Datos del contacto</h2>
                        <div class="form-group">
                            <div class="form-row form-row-1">
                                <input type="text" name="txtNombre" id="txtNombre" class="input-text" placeholder="Nombre(s)" required>
                            </div>
                            <div class="form-row form-row-2">
                                <input type="text" name="txtApellidoPaterno" id="txtApellidoPaterno" class="input-text" placeholder="Apellido paterno" required>
                                <input type="text" name="txtApellidoMaterno" id="txtApellidoMaterno" class="input-text" placeholder="Apellido materno" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <input type="text" name="txtCorreo" id="txtCorreo" class="input-text" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" placeholder="Correo @">
                        </div>
                        <div class="form-row form-row-2">
                            <input type="text" name="txtTelefono" class="input-text" id="txtTelefono" placeholder="Telefono" required>
                        </div>
                        <div class="form-row">

                        </div>
                        <div class="form-group">
                            <div class="form-row form-row-3">
                                <label>&nbsp;</label><input type="text" name="txtAlias" id="txtAlias" class="input-text" placeholder="Alias" required>
                            </div>
                            <div class="form-row form-row-4" style="color:gray; font-size: 15px;text-align: center">
                                <label>Fecha de nacimiento</label><input type="date" style="color:gray; font-size: 12px" name="txtFechaNacimiento" id="txtFechaNacimiento" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row form-row-1">
                                <input type="text" name="txtFacebook" id="txtFacebook" class="input-text" placeholder="Facebook" required>
                            </div>
                            <div class="form-row form-row-2">
                                <input type="text" name="txtInstagram" id="txtInstagram" class="input-text" placeholder="Instagram" required>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-right">
                        <h2>Datos de Emergencia</h2>
                        <div class="form-group">
                            <div class="form-row form-row-1">
                                <input type="text" name="txtNombreEmergencia" id="txtNombreEmergencia" class="input-text" placeholder="Nombre(s)" required>
                            </div>
                            <div class="form-row form-row-2">
                                <input type="text" name="txtApellidoPaternoEmergencia" id="txtApellidoPaternoEmergencia" class="input-text" placeholder="Apellido paterno" required>
                                <input type="text" name="txtApellidoMaternoEmergencia" id="txtApellidoMaternoEmergencia" class="input-text" placeholder="Apellido materno" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <input type="text" name="txtTelefonoEmergencia" class="input-text" id="txtTelefonoEmergencia" placeholder="Telefono de Emergencia" required>
                        </div>
                        <div class="form-row" style="text-align: center">

                            <div class="form-row">
                                <input type="submit" name="register" class="register" value="Registrar Contacto">
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
