<%-- 
    Document   : vistaContactos
    Created on : 24/05/2021, 07:53:37 PM
    Author     : kokob
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>Contactos</title>
    </head>
    <body style="background-color: white">
        <h1>Lista de Contactos</h1>

        <div style="padding: 10px;text-align: center">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido paterno</th>
                        <th scope="col">Apellido materno</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Telefono personal</th>
                        <th scope="col">Telefono emergencia</th>
                        <th scope="col">Nombre emergencia</th>
                        <th scope="col">Apellido paterno Emergencia</th>
                        <th scope="col">Apellido materno Emergencia</th>
                        <th scope="col">Fecha Nacimiento</th>
                        <th scope="col">Alias</th>
                        <th scope="col">Facebook</th>
                        <th scope="col">Instagram</th>
                        <th scope="col" colspan="2">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Josue Isacc</td>
                        <td>Carranza </td>
                        <td>Ramirez</td>
                        <td>isaco.rami@gmail.com</td>
                        <td>5535027799</td>
                        <td>5525244831</td>
                        <td>Edith Elizabeth</td>
                        <td>Ramirez </td>
                        <td>Martinez</td>
                        <td>13/05/2000</td>
                        <td>Isaco</td>
                        <td>Isaco Ramirez</td>
                        <td>IsacoRamirez</td>
                        <td><button type="button" class="btn btn-success">Editar</button></td>
                        <td><button type="button" class="btn btn-danger">Eliminar</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
