/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.modelo;

/**
 *
 * @author kokob
 */
public class Contacto {

    private int id_contacto;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    private String correo;
    private String telefono_personal;
    private String telefono_emergencia;
    private String nombre_emergencia;
    private String apellido_p_emergencia;
    private String apellido_m_emergencia;
    private String fecha_nacimiento;
    private String alias;
    private String facebook;
    private String instagram;
    private String create_at;
    private String update_at;

    public Contacto() {
    }

    public Contacto(int id_contacto, String nombre, String apellido_paterno, String apellido_materno, String correo, String telefono_personal, String telefono_emergencia, String nombre_emergencia, String apellido_p_emergencia, String apellido_m_emergencia, String fecha_nacimiento, String alias, String facebook, String instagram, String create_at, String update_at) {
        this.id_contacto = id_contacto;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.correo = correo;
        this.telefono_personal = telefono_personal;
        this.telefono_emergencia = telefono_emergencia;
        this.nombre_emergencia = nombre_emergencia;
        this.apellido_p_emergencia = apellido_p_emergencia;
        this.apellido_m_emergencia = apellido_m_emergencia;
        this.fecha_nacimiento = fecha_nacimiento;
        this.alias = alias;
        this.facebook = facebook;
        this.instagram = instagram;
        this.create_at = create_at;
        this.update_at = update_at;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono_personal() {
        return telefono_personal;
    }

    public void setTelefono_personal(String telefono_personal) {
        this.telefono_personal = telefono_personal;
    }

    public String getTelefono_emergencia() {
        return telefono_emergencia;
    }

    public void setTelefono_emergencia(String telefono_emergencia) {
        this.telefono_emergencia = telefono_emergencia;
    }

    public String getNombre_emergencia() {
        return nombre_emergencia;
    }

    public void setNombre_emergencia(String nombre_emergencia) {
        this.nombre_emergencia = nombre_emergencia;
    }

    public String getApellido_p_emergencia() {
        return apellido_p_emergencia;
    }

    public void setApellido_p_emergencia(String apellido_p_emergencia) {
        this.apellido_p_emergencia = apellido_p_emergencia;
    }

    public String getApellido_m_emergencia() {
        return apellido_m_emergencia;
    }

    public void setApellido_m_emergencia(String apellido_m_emergencia) {
        this.apellido_m_emergencia = apellido_m_emergencia;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
    
    
    
    
    
    
}
