/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

@Repository
public class ContactosDAO {

    private JdbcTemplate jdbcTmpl;

    private void setJdbcTmpl(JdbcTemplate jdbc) {
        System.out.println("asignación de la conexión");
        this.jdbcTmpl = jdbc;
    }//set

    public int guardar(Contacto c) {
        String sql = "INSERT INTO `usuario_contactos`(`nombre`, `apellido_paterno`, `apellido_materno`, `correo`, `telefono_personal`, `telefono_emergencia`, `nombre_emergencia`, `apellido_p_emergencia`, `apellido_m_emergencia`, `fecha_nacimiento`, `alias`, `facebook`, `instagram`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTmpl.update(sql,
                c.getNombre(),
                c.getApellido_paterno(),
                c.getApellido_materno(),
                c.getCorreo(),
                c.getTelefono_personal(),
                c.getTelefono_emergencia(),
                c.getNombre_emergencia(),
                c.getApellido_p_emergencia(),
                c.getApellido_m_emergencia(),
                c.getFecha_nacimiento(),
                c.getAlias(),
                c.getFacebook(),
                c.getInstagram()
        );
    }//guardar

    public int eleminar(int id) {
        String sql = "delete from usuario_contactos where id_contacto = ?";
        return jdbcTmpl.update(sql, id);
    }//eliminar

    public int modificar(Contacto c) {
        String sql = "UPDATE `usuario_contactos` SET `nombre`=?,`apellido_paterno`=?,`apellido_materno`=?,`correo`=?,`telefono_personal`=?,`telefono_emergencia`=?,`nombre_emergencia`=?,`apellido_p_emergencia`=?,`apellido_m_emergencia`=?,`fecha_nacimiento`=?,`alias`=?,`facebook`=?,`instagram`=? WHERE id_contacto=? ";
        return jdbcTmpl.update(sql,
                c.getNombre(),
                c.getApellido_paterno(),
                c.getApellido_materno(),
                c.getCorreo(),
                c.getTelefono_personal(),
                c.getTelefono_emergencia(),
                c.getNombre_emergencia(),
                c.getApellido_p_emergencia(),
                c.getApellido_m_emergencia(),
                c.getFecha_nacimiento(),
                c.getAlias(),
                c.getFacebook(),
                c.getInstagram(),
                c.getId_contacto()
        );
    }//modificar
    
    
    public List<Contacto> getArticulos() {
        String sql = "select * from usuario_contactos";
        return this.jdbcTmpl.query(sql, new RowMapper<Contacto>() {
            @Override
            //while(rs.next())
            public Contacto mapRow(ResultSet rs, int i) throws SQLException {
                Contacto c = new Contacto();
                c.setId_contacto(rs.getInt("1"));
                c.setNombre(rs.getString("2"));
                c.setApellido_paterno(rs.getString("3"));
                c.setApellido_materno(rs.getString("4"));
                c.setCorreo(rs.getString("5"));
                c.setTelefono_personal(rs.getString("6"));
                c.setTelefono_emergencia(rs.getString("7"));
                c.setNombre_emergencia(rs.getString("8"));
                c.setApellido_p_emergencia(rs.getString("9"));
                c.setApellido_m_emergencia(rs.getString("10"));
                c.setFecha_nacimiento(rs.getString("11"));
                c.setAlias(rs.getString("12"));
                c.setFacebook(rs.getString("13"));
                c.setInstagram(rs.getString("14"));
                c.setCreate_at(rs.getString("15"));
                c.setUpdate_at(rs.getString("16"));
                return c;
            }
        });
    }//getarticulos 
    
    
    
    public Contacto getContactoById(int id) {
        String sql = "select * from usuario_contactos where id_contacto = ?";
        return jdbcTmpl.queryForObject(sql, new Object[]{id},
                new BeanPropertyRowMapper<Contacto>(Contacto.class));
    }//byID   

}
